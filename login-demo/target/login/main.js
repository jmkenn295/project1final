(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n\r\n<!--<button (click) = \"refresh()\">Refresh</button>-->\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/nav/nav.component */ "./src/app/components/nav/nav.component.ts");
/* harmony import */ var _components_view_reimbursements_view_reimbursements_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/view-reimbursements/view-reimbursements.component */ "./src/app/components/view-reimbursements/view-reimbursements.component.ts");
/* harmony import */ var _components_reimbursement_request_reimbursement_request_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/reimbursement-request/reimbursement-request.component */ "./src/app/components/reimbursement-request/reimbursement-request.component.ts");
/* harmony import */ var _components_approve_or_deny_approve_or_deny_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/approve-or-deny/approve-or-deny.component */ "./src/app/components/approve-or-deny/approve-or-deny.component.ts");
/* harmony import */ var _components_view_all_reimbursements_view_all_reimbursements_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/view-all-reimbursements/view-all-reimbursements.component */ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: '',
        component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    },
    {
        path: 'login',
        component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"]
    },
    {
        path: 'reimbursementrequest',
        component: _components_reimbursement_request_reimbursement_request_component__WEBPACK_IMPORTED_MODULE_12__["ReimbursementRequestComponent"]
    },
    {
        path: 'showreimbursementsemp',
        component: _components_view_reimbursements_view_reimbursements_component__WEBPACK_IMPORTED_MODULE_11__["ViewReimbursementsComponent"]
    },
    {
        path: 'showreimbursementsmanager',
        component: _components_view_all_reimbursements_view_all_reimbursements_component__WEBPACK_IMPORTED_MODULE_14__["ViewAllReimbursementsComponent"]
    },
    {
        path: 'approveordenyreimbursements',
        component: _components_approve_or_deny_approve_or_deny_component__WEBPACK_IMPORTED_MODULE_13__["ApproveOrDenyComponent"]
    },
    {
        path: '**',
        redirectTo: ''
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"],
                _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_10__["NavComponent"],
                _components_view_reimbursements_view_reimbursements_component__WEBPACK_IMPORTED_MODULE_11__["ViewReimbursementsComponent"],
                _components_reimbursement_request_reimbursement_request_component__WEBPACK_IMPORTED_MODULE_12__["ReimbursementRequestComponent"],
                _components_approve_or_deny_approve_or_deny_component__WEBPACK_IMPORTED_MODULE_13__["ApproveOrDenyComponent"],
                _components_view_all_reimbursements_view_all_reimbursements_component__WEBPACK_IMPORTED_MODULE_14__["ViewAllReimbursementsComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(routes),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            providers: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_4__["HashLocationStrategy"] }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/approve-or-deny/approve-or-deny.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/approve-or-deny/approve-or-deny.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/approve-or-deny/approve-or-deny.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/approve-or-deny/approve-or-deny.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" id=\"manager-dashboard-container\">\n  <h2>Reimbursements</h2>\n  <table id=\"manager-reimbursements\" class=\"table\">\n    <tr>\n      <th scope=\"col\">Submitted</th>\n      <th scope=\"col\">Amount</th>\n      <th scope=\"col\">Type</th>\n      <th scope=\"col\">Status</th>\n      <th scope=\"col\"></th>\n    </tr>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/components/approve-or-deny/approve-or-deny.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/approve-or-deny/approve-or-deny.component.ts ***!
  \*************************************************************************/
/*! exports provided: ApproveOrDenyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveOrDenyComponent", function() { return ApproveOrDenyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ApproveOrDenyComponent = /** @class */ (function () {
    function ApproveOrDenyComponent() {
    }
    ApproveOrDenyComponent.prototype.ngOnInit = function () {
    };
    ApproveOrDenyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-approve-or-deny',
            template: __webpack_require__(/*! ./approve-or-deny.component.html */ "./src/app/components/approve-or-deny/approve-or-deny.component.html"),
            styles: [__webpack_require__(/*! ./approve-or-deny.component.css */ "./src/app/components/approve-or-deny/approve-or-deny.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ApproveOrDenyComponent);
    return ApproveOrDenyComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-nav [navItems]=\"userNavs\"></app-nav>"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(authService) {
        this.authService = authService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        if (this.authService.hasRoles(['Manager'])) {
            this.userNavs = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].managerNav;
        }
        else {
            this.userNavs = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].employeeNav;
        }
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-container {\r\n    margin: 0 auto;\r\n    width: 100%;\r\n    text-align: center;\r\n}"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-container\">\r\n  <form #loginForm=\"ngForm\" novalidate (ngSubmit)=\"loginSubmit()\">\r\n    <div>\r\n      <input \r\n      type=\"text\"\r\n      name=\"username\" \r\n      placeholder=\"email\" \r\n      [(ngModel)]=\"username\"\r\n      required/>\r\n    </div>\r\n    <div>\r\n      <input \r\n        type=\"password\" \r\n        name=\"password\"\r\n        placeholder=\"password\"\r\n        [(ngModel)]=\"password\"\r\n        required/>\r\n    </div>\r\n    <div>\r\n      <button type=\"submit\" [disabled]=\"!loginForm.valid\">Login</button>\r\n    </div>\r\n  </form>    \r\n</div>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router, route) {
        this.authService = authService;
        this.router = router;
        this.route = route;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
    };
    LoginComponent.prototype.loginSubmit = function () {
        var _this = this;
        this.authService.authenticate(this.username, this.password, function () { return _this.router.navigate([_this.returnUrl]); }, function (err) {
            console.log(err);
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/nav/nav.component.css":
/*!**************************************************!*\
  !*** ./src/app/components/nav/nav.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\r\n    list-style: none;\r\n    border-bottom: 1px solid #bbbb;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n\r\nli {\r\n    display: inline-block;\r\n    margin-right: 20px;\r\n}"

/***/ }),

/***/ "./src/app/components/nav/nav.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/nav/nav.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\r\n  <ul>\r\n    <li *ngFor=\"let item of navItems\">\r\n      <a [routerLink]=\"['/', item.link]\"> {{item.title}} </a>\r\n    </li>\r\n  </ul>\r\n\r\n  <div>\r\n    <button type=\"submit\" (click)=\"Logout()\">Logout</button>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/nav/nav.component.ts ***!
  \*************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavComponent = /** @class */ (function () {
    function NavComponent(authService, router, route) {
        this.authService = authService;
        this.router = router;
        this.route = route;
    }
    NavComponent.prototype.ngOnInit = function () {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    NavComponent.prototype.Logout = function () {
        localStorage.removeItem('authToken');
        this.router.navigate([this.returnUrl]);
        window.location.reload();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], NavComponent.prototype, "navItems", void 0);
    NavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! ./nav.component.html */ "./src/app/components/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/components/nav/nav.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/components/reimbursement-request/reimbursement-request.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/reimbursement-request/reimbursement-request.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input.ng-invalid.ng-touched{\r\n   border: 1px solid red;\r\n }\r\n"

/***/ }),

/***/ "./src/app/components/reimbursement-request/reimbursement-request.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/reimbursement-request/reimbursement-request.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2\">\r\n      <form (ngSubmit)=\"submitRequest(f)\" #f=\"ngForm\">\r\n        <div id=\"reimbursement-data\">\r\n          <div class=\"form-group\">\r\n            <label for=\"reimbursement-amount\">Amount</label>\r\n            <input type=\"number\" id=\"reimbursement-amount\" class=\"form-control\" ngModel name=\"reimbursement-amount\"\r\n                   required>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"reimbursement-description\">Description</label>\r\n            <input type=\"text\" id=\"reimbursement-description\" class=\"form-control\" ngModel\r\n                   name=\"reimbursement-description\" required>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"reimbursement-type\">Type</label>\r\n            <select id=\"reimbursement-type\" class=\"form-control\" [ngModel]=\"defaultTypeOption\" name=\"reimbursement-type\">\r\n              <option value=\"1\">Lodging</option>\r\n              <option value=\"2\">Travel</option>\r\n              <option value=\"3\">Food</option>\r\n              <option value=\"4\">Other</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n        <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Submit</button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/reimbursement-request/reimbursement-request.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/reimbursement-request/reimbursement-request.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ReimbursementRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReimbursementRequestComponent", function() { return ReimbursementRequestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/reimbursement.service */ "./src/app/services/reimbursement.service.ts");
/* harmony import */ var _models_Reimbursement__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/Reimbursement */ "./src/app/models/Reimbursement.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReimbursementRequestComponent = /** @class */ (function () {
    function ReimbursementRequestComponent(reimbursementService, authService) {
        this.reimbursementService = reimbursementService;
        this.authService = authService;
        this.defaultTypeOption = '4';
    }
    ReimbursementRequestComponent.prototype.ngOnInit = function () {
    };
    ReimbursementRequestComponent.prototype.submitRequest = function (form) {
        var reimbursement = new _models_Reimbursement__WEBPACK_IMPORTED_MODULE_2__["Reimbursement"]();
        reimbursement.description = form.value['reimbursement-description'];
        reimbursement.amount = form.value['reimbursement-amount'];
        reimbursement.typeId = parseInt(form.value['reimbursement-type']);
        reimbursement.authorId = this.authService.getLoggedInUser().id;
        reimbursement.resolverId;
        reimbursement.dateResolved = null;
        reimbursement.dateSubmitted = new Date();
        reimbursement.statusId = 1;
        this.reimbursementService.addReimbursement(reimbursement)
            .subscribe(function () { return console.log('success'); }, function () { return console.log('failure'); });
    };
    ReimbursementRequestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reimbursement-request',
            template: __webpack_require__(/*! ./reimbursement-request.component.html */ "./src/app/components/reimbursement-request/reimbursement-request.component.html"),
            styles: [__webpack_require__(/*! ./reimbursement-request.component.css */ "./src/app/components/reimbursement-request/reimbursement-request.component.css")]
        }),
        __metadata("design:paramtypes", [_services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__["ReimbursementService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], ReimbursementRequestComponent);
    return ReimbursementRequestComponent;
}());



/***/ }),

/***/ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/components/view-all-reimbursements/view-all-reimbursements.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/view-all-reimbursements/view-all-reimbursements.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Reimbursement Details</h1>\n\n<ol>\n  <li *ngFor=\"let reimbursement of reimbursements\">\n    <div>\n      {{reimbursement.authorId}}\n    </div>\n    <div>\n      {{reimbursement.description}}\n    </div>\n    <div>\n      {{reimbursement.amount}}\n    </div>\n    <div>\n      {{reimbursement.dateSubmitted}}\n    </div>\n    <div>\n      {{reimbursement.dateResolved}}\n    </div>\n    <div>\n      {{reimbursement.typeId}}\n    </div>\n    <div>\n      {{reimbursement.statusId}}\n    </div>\n    <div>\n      {{reimbursement.resolverId}}\n    </div>\n\n  </li>\n</ol>\n"

/***/ }),

/***/ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/view-all-reimbursements/view-all-reimbursements.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ViewAllReimbursementsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewAllReimbursementsComponent", function() { return ViewAllReimbursementsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/reimbursement.service */ "./src/app/services/reimbursement.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewAllReimbursementsComponent = /** @class */ (function () {
    function ViewAllReimbursementsComponent(reimbursementService) {
        this.reimbursementService = reimbursementService;
    }
    ViewAllReimbursementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.reimbursementService.getAllReimbursements()
            .subscribe(function (reimbursements) {
            _this.reimbursements = reimbursements;
        });
    };
    ViewAllReimbursementsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-view-all-reimbursements',
            template: __webpack_require__(/*! ./view-all-reimbursements.component.html */ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.html"),
            styles: [__webpack_require__(/*! ./view-all-reimbursements.component.css */ "./src/app/components/view-all-reimbursements/view-all-reimbursements.component.css")]
        }),
        __metadata("design:paramtypes", [_services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__["ReimbursementService"]])
    ], ViewAllReimbursementsComponent);
    return ViewAllReimbursementsComponent;
}());



/***/ }),

/***/ "./src/app/components/view-reimbursements/view-reimbursements.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/view-reimbursements/view-reimbursements.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/view-reimbursements/view-reimbursements.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/view-reimbursements/view-reimbursements.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Reimbursement Details</h1>\n\n<ol>\n  <li *ngFor=\"let reimbursement of reimbursements\">\n    <div>\n      {{reimbursement.authorId}}\n    </div>\n    <div>\n    {{reimbursement.description}}\n    </div>\n    <div>\n      {{reimbursement.amount}}\n    </div>\n    <div>\n      {{reimbursement.dateSubmitted}}\n    </div>\n    <div>\n      {{reimbursement.dateResolved}}\n    </div>\n    <div>\n      {{reimbursement.typeId}}\n    </div>\n    <div>\n      {{reimbursement.statusId}}\n    </div>\n    <div>\n      {{reimbursement.resolverId}}\n    </div>\n\n\n  </li>\n</ol>\n"

/***/ }),

/***/ "./src/app/components/view-reimbursements/view-reimbursements.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/view-reimbursements/view-reimbursements.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ViewReimbursementsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewReimbursementsComponent", function() { return ViewReimbursementsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/reimbursement.service */ "./src/app/services/reimbursement.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewReimbursementsComponent = /** @class */ (function () {
    function ViewReimbursementsComponent(reimbursementService, authService) {
        this.reimbursementService = reimbursementService;
        this.authService = authService;
    }
    ViewReimbursementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var loggedInUserId = this.authService.getLoggedInUser().id;
        this.reimbursementService.getReimbursementsByAuthorId(loggedInUserId)
            .subscribe(function (reimbursements) { return _this.reimbursements = reimbursements; });
    };
    ViewReimbursementsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-view-reimbursements',
            template: __webpack_require__(/*! ./view-reimbursements.component.html */ "./src/app/components/view-reimbursements/view-reimbursements.component.html"),
            styles: [__webpack_require__(/*! ./view-reimbursements.component.css */ "./src/app/components/view-reimbursements/view-reimbursements.component.css")]
        }),
        __metadata("design:paramtypes", [_services_reimbursement_service__WEBPACK_IMPORTED_MODULE_1__["ReimbursementService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], ViewReimbursementsComponent);
    return ViewReimbursementsComponent;
}());



/***/ }),

/***/ "./src/app/models/Reimbursement.ts":
/*!*****************************************!*\
  !*** ./src/app/models/Reimbursement.ts ***!
  \*****************************************/
/*! exports provided: Reimbursement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reimbursement", function() { return Reimbursement; });
var Reimbursement = /** @class */ (function () {
    function Reimbursement() {
    }
    return Reimbursement;
}());



/***/ }),

/***/ "./src/app/services/auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(router) {
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('authToken')) {
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
        // used as a route guard.
        // if returning false the route will not be loaded
        ,
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.authUrl = 'http://localhost:8080/login/auth';
    }
    AuthService.prototype.isLoggedIn = function () {
        return localStorage.getItem('authToken') !== null;
    };
    AuthService.prototype.getLoggedInUser = function () {
        var userJson = localStorage.getItem('authToken');
        if (userJson === null)
            return null;
        var user = JSON.parse(userJson);
        return user;
    };
    AuthService.prototype.hasRoles = function (roles) {
        var tokenRoles = JSON.parse(localStorage.getItem('authToken')).roles;
        return roles.every(function (role) { return tokenRoles.indexOf(role) >= 0; });
    };
    AuthService.prototype.authenticate = function (username, password, success, fail) {
        return this.http.post(this.authUrl, JSON.stringify({ email: username, password: password }), {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .toPromise()
            .then(function (resp) {
            localStorage.setItem('authToken', JSON.stringify(resp));
            success();
        }, function (err) {
            fail(err);
        });
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/reimbursement.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/reimbursement.service.ts ***!
  \***************************************************/
/*! exports provided: ReimbursementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReimbursementService", function() { return ReimbursementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReimbursementService = /** @class */ (function () {
    function ReimbursementService(httpClient) {
        this.httpClient = httpClient;
    }
    ReimbursementService.prototype.getReimbursementsByAuthorId = function (authorId) {
        return this.httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/login/reimbursementsbyauthor/' + authorId);
    };
    ReimbursementService.prototype.getAllReimbursements = function () {
        return this.httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/login/reimbursements/');
    };
    ReimbursementService.prototype.addReimbursement = function (reimbursement) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/login/reimbursements/', reimbursement, httpOptions);
    };
    ReimbursementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ReimbursementService);
    return ReimbursementService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: "http://localhost:8080",
    managerNav: [
        {
            title: 'Reimbursements',
            link: 'showreimbursementsmanager'
        },
        {
            title: 'Approve/Deny',
            link: 'approveordenyreimbursements'
        }
    ],
    employeeNav: [
        {
            title: 'Reimbursements',
            link: 'showreimbursementsemp'
        },
        {
            title: 'New Reimbursement',
            link: 'reimbursementrequest'
        }
    ]
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\jmken\Desktop\ClassGit5\spark_nov27_usf\Week7_J2EE_Angular\login-demo\login-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
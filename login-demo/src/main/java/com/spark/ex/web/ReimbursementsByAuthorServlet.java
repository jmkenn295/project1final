package com.spark.ex.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spark.ex.pojos.Reimbursement;
import com.spark.ex.services.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public class ReimbursementsByAuthorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get the author id from the uri
        ObjectMapper mapper = new ObjectMapper();
        String uri = request.getRequestURI();
        String[] uriArray = uri.split("/");
        int authorId = Integer.parseInt(uriArray[uriArray.length - 1]);
        ReimbursementService reimbursementService = new ReimbursementService();
        List<Reimbursement> reimbursementList = null;



        //call the reimbursement service to get the reimbursements for that author
        reimbursementList = reimbursementService.getReimbursementsByAuthorId(authorId);

        //write the reimbursements to the response
        PrintWriter pw = response.getWriter();
        response.setContentType("application/json");
        String reimbursementsListJSON = mapper.writeValueAsString(reimbursementList);
        pw.write(reimbursementsListJSON);
    }
}
